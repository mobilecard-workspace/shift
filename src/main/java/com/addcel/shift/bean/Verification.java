package com.addcel.shift.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Verification {

	private String verification_source;

	public String getVerification_source() {
		return verification_source;
	}

	public void setVerification_source(String verification_source) {
		this.verification_source = verification_source;
	}
	
}
