package com.addcel.shift.bean;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Card {

	@SerializedName("account_id")
	private String accountId;
	
	@SerializedName("last_four")
	private String lastFour;
	
	@SerializedName("card_network")
	private String cardNetwork;
	
	@SerializedName("card_issuer")
	private String cardIssuer;
	
	@SerializedName("card_brand")
	private String cardBrand;
	
	private String expiration;
	
	private String pan;
	
	private String cvv;
	
	private String state;
	
	@SerializedName("kyc_status")
	private String kycStatus;
	
	@SerializedName("kyc_reason")
	private List kycReason;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getLastFour() {
		return lastFour;
	}

	public void setLastFour(String lastFour) {
		this.lastFour = lastFour;
	}

	public String getCardNetwork() {
		return cardNetwork;
	}

	public void setCardNetwork(String cardNetwork) {
		this.cardNetwork = cardNetwork;
	}

	public String getCardIssuer() {
		return cardIssuer;
	}

	public void setCardIssuer(String cardIssuer) {
		this.cardIssuer = cardIssuer;
	}

	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getExpiration() {
		return expiration;
	}

	public void setExpiration(String expiration) {
		this.expiration = expiration;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}

	public List getKycReason() {
		return kycReason;
	}

	public void setKycReason(List kycReason) {
		this.kycReason = kycReason;
	}
}
