package com.addcel.shift.bean;

import com.google.gson.annotations.SerializedName;

public class User {

	private String type;
	
	@SerializedName("user_id")
	private String userId;
	
	@SerializedName("user_token")
	private String userToken;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
	
}
