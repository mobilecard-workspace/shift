package com.addcel.shift.bean;

public class ShiftConfig {

	private long id;
	private String headerDeveloper;
	private String headerProject;
	private String headerPartnerAuthorization;
	private String headerPartnerId;
	private String endpointShift;
	private String endpointShitfPartner;

	public ShiftConfig() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHeaderDeveloper() {
		return headerDeveloper;
	}

	public void setHeaderDeveloper(String headerDeveloper) {
		this.headerDeveloper = headerDeveloper;
	}

	public String getHeaderProject() {
		return headerProject;
	}

	public void setHeaderProject(String headerProject) {
		this.headerProject = headerProject;
	}

	public String getHeaderPartnerAuthorization() {
		return headerPartnerAuthorization;
	}

	public void setHeaderPartnerAuthorization(String headerPartnerAuthorization) {
		this.headerPartnerAuthorization = headerPartnerAuthorization;
	}

	public String getHeaderPartnerId() {
		return headerPartnerId;
	}

	public void setHeaderPartnerId(String headerPartnerId) {
		this.headerPartnerId = headerPartnerId;
	}

	public String getEndpointShift() {
		return endpointShift;
	}

	public void setEndpointShift(String endpointShift) {
		this.endpointShift = endpointShift;
	}

	public String getEndpointShitfPartner() {
		return endpointShitfPartner;
	}

	public void setEndpointShitfPartner(String endpointShitfPartner) {
		this.endpointShitfPartner = endpointShitfPartner;
	}

	

}
