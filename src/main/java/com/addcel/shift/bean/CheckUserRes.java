package com.addcel.shift.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckUserRes {
	
	private int idError;
	private String errorMessage;
    private long idUser;
    private DataPoint  data_points;
    private Card card; 
    
    public CheckUserRes() {
		// TODO Auto-generated constructor stub
	}

    public void setEmpty(){
    	this.data_points = new DataPoint();
    	this.card = new Card();
    }
    
	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	

	public DataPoint getData_points() {
		return data_points;
	}

	public void setData_points(DataPoint data_points) {
		this.data_points = data_points;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}
    
    

}
