package com.addcel.shift.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataPoint {

private String type;

private List<Data> data = null;

public DataPoint() {
	// TODO Auto-generated constructor stub
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

public List<Data> getData() {
	return data;
}

public void setData(List<Data> data) {
	this.data = data;
}


	
}
