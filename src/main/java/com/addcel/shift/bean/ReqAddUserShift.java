package com.addcel.shift.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReqAddUserShift {

	private DataPoint dataPoints;

	public DataPoint getDataPoints() {
		return dataPoints;
	}

	public void setDataPoints(DataPoint dataPoints) {
		this.dataPoints = dataPoints;
	}
	
	
	
}
