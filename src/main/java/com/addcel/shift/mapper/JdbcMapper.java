package com.addcel.shift.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.shift.bean.CardShift;
import com.addcel.shift.bean.ShiftConfig;
import com.addcel.shift.bean.UserShift;



public interface JdbcMapper {
	
	public ShiftConfig getShiftConfig();

	public ShiftConfig getHeadersToken();
	
	public void insertShiftCard(CardShift card);
	
	public void inserShiftUser(UserShift user);
	
	public UserShift getUserShift( @Param(value = "idUsuario") long idUsuario,@Param(value = "idApp")  int idApp);
	
	public CardShift getcardShift( @Param(value = "idUsuario") long idUsuario, @Param(value = "idApp") int idApp, @Param(value = "id_shift") String id_shift);

	public String ProcessCard(HashMap parameters);
	
	
}
