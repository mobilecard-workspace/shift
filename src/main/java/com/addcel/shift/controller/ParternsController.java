package com.addcel.shift.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.addcel.shift.bean.Card;
import com.addcel.shift.bean.CheckUserRes;
import com.addcel.shift.bean.Data;
import com.addcel.shift.bean.DataPoint;
import com.addcel.shift.bean.ReqAddUserShift;
import com.addcel.shift.bean.ShiftConfig;
import com.addcel.shift.bean.User;
import com.addcel.shift.bean.UserCreateReq;
import com.addcel.shift.bean.UsuarioMC;
import com.addcel.shift.bean.Verification;
import com.addcel.shift.services.PartnersServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
public class ParternsController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ParternsController.class);

	private static final String PATH_PARTNERS_USER = "{idApp}/{idPais}/{idioma}/create/user";
	
	private static final String PATH_ACTIVATE_CARD = "{idApp}/{idPais}/{idioma}/activate/card";
	
	private static final String PATH_CREATE_CARD = "{idApp}/{idPais}/{idioma}/create/card";
	
	private static final String PATH_LOGIN_DATA = "{idApp}/{idPais}/{idioma}/login";
	
	private static final String PATH_USER_CHECK = "{idApp}/{idUsuario}/{idPais}/CheckUser";
	
	private static final String PATH_SHIFT_CREDENTIALS = "credentials";
	
	@Autowired
	PartnersServices service;
	
	@RequestMapping(value = PATH_PARTNERS_USER, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<CheckUserRes> createUsers(@PathVariable Integer idApp, @PathVariable Integer idPais, 
			@PathVariable String idioma, @RequestBody UserCreateReq user){
		return new ResponseEntity<CheckUserRes>(service.createUserCard(idApp, idPais, idioma, user), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = PATH_SHIFT_CREDENTIALS)
	public ResponseEntity<ShiftConfig> credentials(){
		return new ResponseEntity<ShiftConfig>(service.credentials(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "testing")
	public ResponseEntity<String> test(){
		try{
		String xmlRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<ValidadorCuenta>"
				+ "<Cuenta tipoIdUsuario=\""+"\" "
						+ "idUsuario=\""+"\" "
						+ "nitUsuario=\""+"\" "
						+ "clave=\""+"\" "
				+ "tipoIdCliente=\""+"\" "
				+ "idCliente=\""+"\" "
				+ "ctaCliente=\""+"\" "
				+ "primerApellido=\""+" "+"\""
				+ " nombreCliente=\""+"\" /></ValidadorCuenta>";
		
		RestTemplate restTemplate =  new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_XML);
	    HttpEntity<String> request = new HttpEntity<String>(xmlRequest, headers);
	    LOGGER.debug("enviando petiucion");
	    ResponseEntity<String> response = restTemplate.postForEntity("http://172.24.14.7:8080/ValidacionWS/services/ServicioValidacion", request, String.class);
		
		
		return new ResponseEntity<String>(response.getBody(), HttpStatus.OK);
		
		}catch(Exception ex){
			LOGGER.debug("Error al enviar peticion", ex);
			return new ResponseEntity<String>(ex.getMessage(),HttpStatus.OK);
		}
	}
	/*@RequestMapping(value = PATH_CREATE_CARD)
	public ResponseEntity<Card> createCard(@PathVariable Integer idApp, @PathVariable Integer idPais, 
			@PathVariable String idioma, @RequestBody UsuarioMC user){
		return new ResponseEntity<Card>(service.issueShitfCard(idApp, idPais, idioma, user), HttpStatus.OK);
	}
	
	@RequestMapping(value = PATH_ACTIVATE_CARD)
	public ResponseEntity<Card> activateCard(@PathVariable Integer idApp, @PathVariable Integer idPais, 
			@PathVariable String idioma, @RequestBody UsuarioMC user){
		return new ResponseEntity<Card>(service.activateCard(idApp, idPais, idioma, user), HttpStatus.OK);
	}*/
	
	@RequestMapping(value=PATH_USER_CHECK, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<CheckUserRes> checkUser(@PathVariable Integer idApp,@PathVariable long idUsuario, @PathVariable Integer idPais,@RequestParam  String idioma, @RequestParam String plataforma){

		return new ResponseEntity<CheckUserRes>(service.checkUser(idApp, idUsuario, idPais, idioma, plataforma),HttpStatus.OK);
	}
}
