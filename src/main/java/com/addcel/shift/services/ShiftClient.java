package com.addcel.shift.services;

import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.addcel.shift.bean.Card;
import com.addcel.shift.bean.ShiftConfig;
import com.addcel.shift.bean.User;
import com.addcel.shift.bean.UserCreateReq;
import com.addcel.shift.bean.UsuarioMC;
import com.addcel.shift.mapper.JdbcMapper;
import com.google.gson.Gson;

@Service
public class ShiftClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShiftClient.class);
	
private static final String HEADER_PREFIXED = "Bearer ";
	
	private static final Gson GSON = new Gson();
	
	@Autowired
	private JdbcMapper jdbc;

	public ShiftConfig credentials() {
		ShiftConfig config = null;
		try {
			LOGGER.info("OBTENIENDO HEADERS PARA SDK...");
			config = jdbc.getHeadersToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config;
	}

	public User createUsers(UserCreateReq userMC, ShiftConfig config) {
		User user = null;
		ResponseEntity response = null;
		try {
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
		
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Developer-Authorization", HEADER_PREFIXED + config.getHeaderDeveloper());
			headers.set("Project", HEADER_PREFIXED + config.getHeaderProject());
			headers.set("Trusted-Partner-Authorization", HEADER_PREFIXED + config.getHeaderPartnerAuthorization());
			headers.set("Trusted-Partner-ID", config.getHeaderPartnerId());
			headers.set("Authorization", "Basic " + new String(Base64.getEncoder().encode("4954f7cd94ea6f8e:3tTX6Dt2GZd5DTPGICeHKEKzuCyR3Rh5VYS8zNJnKkbfZFPJKURd4k7emqpqP8C5".getBytes())));
			headers.set("Content-Type", "application/json");
			
			try{
				if(userMC.getBirthday().split("-")[0].length() == 2){
					SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-YYYY");
					SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
					Date date = formatter.parse(userMC.getBirthday());
					LOGGER.debug("Nuevo Date: " + formatter2.format(date));
					userMC.setBirthday(formatter2.format(date));
				}
			}catch(Exception ex){
				LOGGER.error("ERROR AL FORMATEAR FECHA NACIMEINTO: " + ex);
			}
			
			HttpEntity<String> entity = new HttpEntity<String>("\n" + 
					"{\n" + 
					"    \"data_points\": {\n" + 
					"        \"type\": \"list\", \n" + 
					"        \"data\": [\n" + 
					"            { \n" + 
					"                \"verification\": {\n" + 
					"                    \"verification_source\": \"google\"\n" + 
					"                }, \n" + 
					"                \"phone_number\": \""+userMC.getPhoneNumber()+"\", \n" + 
					"                \"data_type\": \"phone\", \n" + 
					"                \"country_code\": \"1\"\n" + 
					"            }, \n" + 
					"            {\n" +
					"                \"verification\": {\n" + 
					"                    \"verification_source\": \"google\"\n" + 
					"                }, \n" + 
					"                \"email\": \""+userMC.getEmail()+"\", \n" + 
					"                \"data_type\": \"email\"\n" + 
					"            },\n" + 
					"             {\n" + 
					"                \"first_name\": \""+userMC.getFirstName()+"\", \n" + 
					"                \"last_name\":\""+userMC.getLastName()+"\", \n" + 
					"                \"data_type\": \"name\"\n" + 
					"            },\n" + 
					"            {\n" + 
					"            	\"date\":\""+userMC.getBirthday()+"\",\n" + 
					"            	\"data_type\": \"birthdate\"\n" + 
					"            },\n" + 
					"            {\n" + 
					"            	\"data_type\": \"id_document\",\n" + 
					"            	\"doc_type\": \"ssn\",\n" + 
					"            	\"country\": \"US\",\n" + 
					"            	\"value\":\""+userMC.getSsn()+"\"\n" + 
					//"            	\"data_type\": \"ssn\",\n" + 
					//"            	\"ssn\":\""+userMC.getSsn()+"\"\n" + 
					"            },\n" + 
					"            {\n" + 
					"            	\"country\": \""+"US"+"\",\n" + 
					"            	\"street_one\": \""+userMC.getAddress()+"\",\n" + 
					"            	\"street_two\": \""+userMC.getAddress()+"\",\n" + 
					"            	\"locality\": \""+userMC.getCity()+"\",\n" +
					"            	\"region\": \""+userMC.getState()+"\",\n" +
					"            	\"postal_code\": \""+userMC.getZip()+"\",\n" +
					"            	\"data_type\":\"address\"\n" + 
					
					//"            	\"address\": \""+userMC.getAddress()+" \",\n" + 
					//"            	\"apt\":\""+userMC.getApt()+"\",\n" + 
					//"            	\"city\":\""+userMC.getCity()+"\",\n" + 
					//"            	\"state\":\""+userMC.getState()+"\",\n" + 
					//"            	\"zip\":\""+userMC.getZip()+"\",\n" + 
					//"            	\"data_type\":\"address\"\n" + 
					"            }\n" + 
					"        ]\n" + 
					"    }\n" + 
					"}\n", headers);
			LOGGER.info("BODY: "+"\n" + 
					"{\n" + 
					"    \"data_points\": {\n" + 
					"        \"type\": \"list\", \n" + 
					"        \"data\": [\n" + 
					"            { \n" + 
					"                \"verification\": {\n" + 
					"                    \"verification_source\": \"google\"\n" + 
					"                }, \n" + 
					"                \"phone_number\": \""+userMC.getPhoneNumber()+"\", \n" + 
					"                \"data_type\": \"phone\", \n" + 
					"                \"country_code\": \"1\"\n" + 
					"            }, \n" + 
					"            {\n" + 
					"                \"verification\": {\n" + 
					"                    \"verification_source\": \"google\"\n" + 
					"                }, \n" + 
					"                \"email\": \""+userMC.getEmail()+"\", \n" + 
					"                \"data_type\": \"email\"\n" + 
					"            },\n" + 
					"             {\n" + 
					"                \"first_name\": \""+userMC.getFirstName()+"\", \n" + 
					"                \"last_name\":\""+userMC.getLastName()+"\", \n" + 
					"                \"data_type\": \"name\"\n" + 
					"            },\n" + 
					"            {\n" + 
					"            	\"date\":\""+userMC.getBirthday()+"\",\n" + 
					"            	\"data_type\": \"birthdate\"\n" + 
					"            },\n" + 
					"            {\n" + 
					"            	\"data_type\": \"id_document\",\n" + 
					"            	\"doc_type\": \"ssn\",\n" + 
					"            	\"country\": \"US\",\n" + 
					"            	\"value\":\""+userMC.getSsn()+"\"\n" + 
					//"            	\"data_type\": \"ssn\",\n" + 
					//"            	\"ssn\":\""+userMC.getSsn()+"\"\n" + 
					"            },\n" + 
					"            {\n" + 
					"            	\"country\": \""+"US"+"\",\n" + 
					"            	\"street_one\": \""+userMC.getAddress()+"\",\n" + 
					"            	\"street_two\": \""+userMC.getAddress()+"\",\n" + 
					"            	\"locality\": \""+userMC.getCity()+"\",\n" +
					"            	\"region\": \""+userMC.getState()+"\",\n" +
					"            	\"postal_code\": \""+userMC.getZip()+"\",\n" +
					"            	\"data_type\":\"address\"\n" + 
					//"            	\"address\": \""+userMC.getAddress()+" \",\n" + 
					//"            	\"apt\":\""+userMC.getApt()+"\",\n" + 
					//"            	\"city\":\""+userMC.getCity()+"\",\n" + 
					//"            	\"state\":\""+userMC.getState()+"\",\n" + 
					//"            	\"zip\":\""+userMC.getZip()+"\",\n" + 
					//"            	\"data_type\":\"address\"\n" + 
					"            }\n" + 
					"        ]\n" + 
					"    }\n" + 
					"}\n");
			
			//https://sbx.ledge.me/v1
			response = rest.exchange("https://vault.sbx.ledge.me/v1/partner/users", HttpMethod.POST, entity, String.class);
			//response = rest.exchange("https://sbx.ledge.me/v1/partner/users", HttpMethod.POST, entity, String.class);
			LOGGER.info("RESPONSE FROM SHIFT: {}", response.getBody());
			user = (User) GSON.fromJson((String) response.getBody(), User.class);
		}catch(HttpClientErrorException ex){
			LOGGER.error("ERROR RESPONSE FROM SHIFT : " + ex.getStatusCode().value() + " " + ex.getResponseBodyAsString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public Card issueShitfCard(String token) {
		ShiftConfig config = null;
		ResponseEntity response = null;
		Card card = null;
		try {
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			config = jdbc.getHeadersToken();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Developer-Authorization", HEADER_PREFIXED + config.getHeaderDeveloper());
			headers.set("Project", HEADER_PREFIXED + config.getHeaderProject());
			headers.set("Trusted-Partner-Authorization", HEADER_PREFIXED + config.getHeaderPartnerAuthorization());
			headers.set("Trusted-Partner-ID", config.getHeaderPartnerId());
			headers.set("Authorization", "Bearer " + token); //token
			headers.set("Api-Key","Bearer " + "vSGISmhIDNBJR6UMsgy7v9NjeAJUo/88G82i++lRPXckgAt+BgeWgcc6+B5lByYC");
			     
			//headers.set("Authorization", "Basic " + new String(Base64.getEncoder().encode("4954f7cd94ea6f8e:3tTX6Dt2GZd5DTPGICeHKEKzuCyR3Rh5VYS8zNJnKkbfZFPJKURd4k7emqpqP8C5".getBytes())));
			LOGGER.debug("Headers: ");
			LOGGER.debug("Accept " + MediaType.APPLICATION_JSON_VALUE);
			LOGGER.debug("Developer-Authorization " + HEADER_PREFIXED + config.getHeaderDeveloper());
			LOGGER.debug("Project "+ HEADER_PREFIXED + config.getHeaderProject());
			LOGGER.debug("Trusted-Partner-Authorization "+ HEADER_PREFIXED + config.getHeaderPartnerAuthorization());
			LOGGER.debug("Trusted-Partner-ID "+ config.getHeaderPartnerId());
			LOGGER.debug("Authorization "+ "Bearer " + token);
			
			headers.set("Content-Type", "application/json");
			HttpEntity<String> entity = new HttpEntity<String>("{\"issuer\":\"shift\"}", headers);			
			response = rest.exchange("https://vault.sbx.ledge.me/v1/user/accounts/issuecard", HttpMethod.POST, entity, String.class);			
			LOGGER.info("RESPONSE FROM SHIFT: {}", response.getBody());
			card = (Card) GSON.fromJson((String) response.getBody(), Card.class);
		}catch(HttpClientErrorException ex){
			LOGGER.error("ERROR RESPONSE FROM SHIFT : " + ex.getStatusCode().value() + " " + ex.getResponseBodyAsString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return card;
	}

	public Card activateCard( String token, String AccountId) {
		ShiftConfig config = null;
		boolean status;
		ResponseEntity response = null;
		Card card = null;
		try {
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			config = jdbc.getHeadersToken();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Developer-Authorization", HEADER_PREFIXED + config.getHeaderDeveloper());
			headers.set("Project", HEADER_PREFIXED + config.getHeaderProject());
			headers.set("Trusted-Partner-Authorization", HEADER_PREFIXED + config.getHeaderPartnerAuthorization());
			headers.set("Trusted-Partner-ID", config.getHeaderPartnerId());
			//headers.set("Authorization", token);
			
			headers.set("Authorization", "Bearer " + token); //token
			headers.set("Api-Key","Bearer " + "vSGISmhIDNBJR6UMsgy7v9NjeAJUo/88G82i++lRPXckgAt+BgeWgcc6+B5lByYC");
			 
			//headers.set("Authorization", "Basic " + new String(Base64.getEncoder().encode("4954f7cd94ea6f8e:3tTX6Dt2GZd5DTPGICeHKEKzuCyR3Rh5VYS8zNJnKkbfZFPJKURd4k7emqpqP8C5".getBytes())));
			
			headers.set("Content-Type", "application/json");
			
			LOGGER.debug("Headers: ");
			LOGGER.debug("Accept " + MediaType.APPLICATION_JSON_VALUE);
			LOGGER.debug("Developer-Authorization " + HEADER_PREFIXED + config.getHeaderDeveloper());
			LOGGER.debug("Project "+ HEADER_PREFIXED + config.getHeaderProject());
			LOGGER.debug("Trusted-Partner-Authorization "+ HEADER_PREFIXED + config.getHeaderPartnerAuthorization());
			LOGGER.debug("Trusted-Partner-ID "+ config.getHeaderPartnerId());
			LOGGER.debug("Authorization "+ token);
			LOGGER.debug("Content-Type "+ "application/json");
			
			HttpEntity<String> entity = new HttpEntity<String>( headers);			//"{\"issuer\":\"shift\"}"
			response = rest.exchange("https://vault.sbx.ledge.me/v1/user/accounts/"+AccountId+"/activate", HttpMethod.POST, entity, String.class);			
			LOGGER.info("RESPONSE FROM SHIFT: {}", response.getBody());
			card = (Card) GSON.fromJson((String) response.getBody(), Card.class);
		}catch(HttpClientErrorException ex){
			LOGGER.error("ERROR RESPONSE FROM SHIFT : " + ex.getStatusCode().value() + " " + ex.getResponseBodyAsString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return card;
	}
	
}
