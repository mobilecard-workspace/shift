package com.addcel.shift.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


import crypto.Crypto;

@Component
public class UtilsService {
	
    private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
    private static final String patron = "dd/MM/yyyy";

    private static final String TelefonoServicio = "5525963513";
    
    


    public static String setSMS(String Telefono) {
        return Crypto.aesEncrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String getSMS(String Telefono) {
        return Crypto.aesDecrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";

        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }

        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }
	
	
	public static boolean isEmpty(String cadena){
		boolean resp = false;
		if(cadena == null){
			resp = true;
		}else if("".equals(cadena)){
			resp = true;
		}
		return resp; 
		
	}
	
       
}
