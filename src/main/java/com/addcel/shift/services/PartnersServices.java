package com.addcel.shift.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.shift.bean.Card;
import com.addcel.shift.bean.CardShift;
import com.addcel.shift.bean.CheckUserRes;
import com.addcel.shift.bean.Data;
import com.addcel.shift.bean.DataPoint;
import com.addcel.shift.bean.ReqAddUserShift;
import com.addcel.shift.bean.ShiftConfig;
import com.addcel.shift.bean.User;
import com.addcel.shift.bean.UserCreateReq;
import com.addcel.shift.bean.UserShift;
import com.addcel.shift.bean.UsuarioMC;
import com.addcel.shift.bean.Verification;
import com.addcel.shift.mapper.JdbcMapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class PartnersServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(PartnersServices.class);
	
	private static final String HEADER_PREFIXED = "Bearer ";
	
	private static final Gson GSON = new Gson();
	
	@Autowired
	private JdbcMapper jdbc;

	@Autowired
	private ShiftClient shiftClient;
	
	public ShiftConfig credentials() {
		ShiftConfig config = null;
		try {
			LOGGER.info("OBTENIENDO HEADERS PARA SDK...");
			config = jdbc.getHeadersToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config;
	}
	
	public CheckUserRes createUserCard(int idApp, int idPais, String idioma, UserCreateReq userReq){
		
		CheckUserRes response = new CheckUserRes();
		User user = null;
		ShiftConfig config = null;
		try{
			
			LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO/TARJETA SHIFT");
			config = jdbc.getHeadersToken();
			user = shiftClient.createUsers(userReq, config);
			
			if(user.getUserId()!= null && !user.getUserId().isEmpty()){
				UserShift userS = new UserShift();
				userS.setAddress(userReq.getAddress());
				userS.setApt(userReq.getApt());
				userS.setBirthday(userReq.getBirthday());
				userS.setCity(userReq.getCity());
				//userS.setDate_up_shift(date_up_shift);
				userS.setEmail(userReq.getEmail());
				userS.setFirst_name(userReq.getFirstName());
				userS.setId_app(idApp);
				userS.setLast_name(userReq.getLastName());
				userS.setPhone_number(userReq.getPhoneNumber());
				userS.setSsn(userReq.getSsn());
				userS.setState(userReq.getState());
				userS.setType(user.getType());
				userS.setUser_id(userReq.getIdUser());
				userS.setUser_id_shift(user.getUserId());
				userS.setUser_token(user.getUserToken());
				userS.setZip(userReq.getZip());
				LOGGER.debug("INSERTANDO USUARIO EN BASE DE DATOS");
				jdbc.inserShiftUser(userS);
				
				LOGGER.debug("CREANDO TARJETA...");
				Card card = shiftClient.issueShitfCard(user.getUserToken());
				LOGGER.debug("ACTIVANDO TARJETA...");
				Card activation = shiftClient.activateCard(user.getUserToken(), card.getAccountId());
				
				LOGGER.debug("INSERTANDO EN WALLET");
				insertWallet(userReq, card, idioma, idApp);
				
				CardShift cardS = new CardShift();
				cardS.setAccount_id(card.getAccountId());
				cardS.setCard_brand(card.getCardBrand());
				cardS.setCard_issuer(card.getCardIssuer());
				cardS.setCard_network(card.getCardNetwork());
				cardS.setCvv(card.getCvv());
				cardS.setExpiration(card.getExpiration());
				cardS.setId_app(idApp);
				cardS.setKyc_l("");
				cardS.setKyc_status(activation == null ?  card.getKycStatus() :  activation.getKycStatus());
				cardS.setKycReason(card.getKycReason().toString());
				cardS.setLast_four(card.getLastFour());
				cardS.setPan(card.getPan());
				cardS.setState(activation == null ? card.getState() :activation.getState());
				cardS.setType("card");
				cardS.setUser_id(userReq.getIdUser());
				cardS.setUser_id_shift(user.getUserId());
				LOGGER.debug("INSERTANDO TARJETA EN BASE DE DATOS...");
				jdbc.insertShiftCard(cardS);
				
				
				DataPoint datapoint =  new DataPoint();
				List<Data> list = new ArrayList<Data>();
				Data data = new Data();
				
				Verification ver = new Verification();
				ver.setVerification_source("google");
				data.setVerification(ver);
				data.setPhone_number(userReq.getPhoneNumber() );
				data.setData_type("phone");
				data.setCountry_code("1");
				
				list.add(data);
				
				data = new Data();
				ver = new Verification();
				ver.setVerification_source("google");
				data.setVerification(ver);
				data.setEmail(userReq.getEmail());
				data.setData_type("email");
				list.add(data);
				
				data = new Data();
				data.setFirst_name(userReq.getEmail());
				data.setLast_name(userReq.getLastName());
				data.setData_type("name");
				list.add(data);
				
				
				data = new Data();
				data.setDate(userReq.getBirthday());
				data.setData_type("birthdate");
				list.add(data);
				
				data = new Data();
				data.setSsn(userReq.getSsn());
				data.setData_type("ssn");
				list.add(data);
				
				data = new Data();
				data.setAddress(userReq.getAddress());
				data.setApt(userReq.getApt());
				data.setCity(userReq.getCity());
				data.setState(userReq.getState());
				data.setZip(userReq.getZip());
				data.setData_type("address");
				list.add(data);
				
				datapoint.setData(list);
				datapoint.setType("list");
				
				if(activation == null ){
					response.setErrorMessage("Error al activar Tarjeta");
					response.setIdError(300);
					response.setCard(card);
				}
				else
				{
					response.setErrorMessage("");
					response.setCard(activation);
					response.setIdError(0);
				}
				
				response.setData_points(datapoint);
				
				
				response.setIdUser(userReq.getIdUser());
				
				
			}else
			{
				response.setEmpty();
				response.setIdError(100);
				if(idioma.equalsIgnoreCase("es"))
					response.setErrorMessage("Error al registrar cuenta");
				else
					response.setErrorMessage("Error registering account");
			}
			
		}catch(Exception ex){
			LOGGER.error("Error al crear usuarios/tarjeta Shift...", ex);
			response.setEmpty();
			response.setIdError(200);
			if(idioma.equalsIgnoreCase("es"))
				response.setErrorMessage("Error inesperado, intente mas tarde");
			else
				response.setErrorMessage("Unexpected error, try later");
		}
		LOGGER.debug("RETORNANDO RESPUESTA: ADD USER/CARD" + GSON.toJson(response));
		return response;
	}

	private void insertWallet(UserCreateReq userReq, Card card,String idioma, int idApp ){
		try{
			
		String exp = card.getExpiration().substring(2).replace("-", "/");	
		LOGGER.debug("exp: " + exp);
		HashMap parameters = new HashMap<>();
		parameters.put("nusuario", userReq.getIdUser());
		parameters.put("idapp",idApp );
		parameters.put("nnumtarjeta", card.getPan());
		parameters.put("nnumtarjetacrip", UtilsService.setSMS(card.getPan()));
		parameters.put("nvigencia", UtilsService.setSMS(exp));
		parameters.put("nproceso", 1);
		parameters.put("nidtarjetausuario", 1);
		parameters.put("nct", UtilsService.setSMS(card.getCvv()));
		parameters.put("ntarjeta", userReq.getFirstName() + " " + userReq.getLastName());
		parameters.put("domamex",userReq.getAddress() );
		parameters.put("cpamex", userReq.getZip());
		parameters.put("idioma", idioma);
		parameters.put("mobilecard", 1);
		parameters.put("tipotarjeta", "CREDITO");
		
		LOGGER.debug("parameters add card: " + parameters.get("nproceso") );
		
		String responseAdd = jdbc.ProcessCard(parameters);
		
		LOGGER.debug("Respuesta de BD "  + responseAdd);
		
		}catch(Exception ex){
			LOGGER.error("Error al agregar tarjeta shift a Wallet ", ex);
		}
	}
	
	
	public CheckUserRes checkUser(Integer idApp, long idUsuario,Integer idPais,String idioma, String plataforma){
		
		CheckUserRes response = new CheckUserRes();
		response.setIdError(0);
		boolean band= false;
		try{
			LOGGER.debug("verificando usuario Shift");
			List<Data> list = new ArrayList<Data>();
			
			ReqAddUserShift req = new ReqAddUserShift();
			
			DataPoint datapoint =  new DataPoint();
			Data data = new Data();
			UserShift user = jdbc.getUserShift(idUsuario, idApp);
			
			if(user == null)
				response.setIdError(99);
			else
				{
					response.setIdError(0);
					band= true;
				}
			
			Verification ver = new Verification();
			ver.setVerification_source("google");
			data.setVerification(ver);
			data.setPhone_number(band ? user.getPhone_number() : "");
			data.setData_type("phone");
			data.setCountry_code("1");
			
			list.add(data);
			
			data = new Data();
			ver = new Verification();
			ver.setVerification_source("google");
			data.setVerification(ver);
			data.setEmail(band ? user.getEmail() : "");
			data.setData_type("email");
			list.add(data);
			
			data = new Data();
			data.setFirst_name(band ? user.getFirst_name() : "");
			data.setLast_name(band ? user.getLast_name() : "");
			data.setData_type("name");
			list.add(data);
			
			
			data = new Data();
			data.setDate(band ? user.getBirthday() : "");
			data.setData_type("birthdate");
			list.add(data);
			
			data = new Data();
			data.setSsn(band ? user.getSsn() : "");
			data.setData_type("ssn");
			list.add(data);
			
			data = new Data();
			data.setAddress(band ? user.getAddress() : "");
			data.setApt(band ? user.getApt() : "");
			data.setCity(band ? user.getCity() : "");
			data.setState(band ? user.getState() : "");
			data.setZip(band ? user.getZip() : "");
			data.setData_type("address");
			list.add(data);
			
			
			datapoint.setData(list);
			datapoint.setType("list");
			
			req.setDataPoints(datapoint);
			
			
			//tarjeta Shift
			CardShift Scard = jdbc.getcardShift(idUsuario, idApp, user == null ? "" : user.getUser_id_shift());
			Card card = new Card();
			card.setAccountId(Scard != null ? Scard.getAccount_id() : "");
			card.setCardBrand(Scard != null ? Scard.getCard_brand() : "");
			card.setCardIssuer(Scard != null ? Scard.getCard_issuer() : "");
			card.setCardNetwork(Scard != null ? Scard.getCard_network() : "");
			card.setCvv(Scard != null ? Scard.getCvv() : "");
			card.setExpiration(Scard != null ? Scard.getExpiration() : "");
			List kycReason = new ArrayList();
			card.setKycReason(kycReason);
			card.setKycStatus(Scard != null ? Scard.getKyc_status() : "");
			card.setLastFour(Scard != null ? Scard.getLast_four() : "");
			card.setPan(Scard != null ? Scard.getPan() : "");
			card.setState(Scard != null ? Scard.getState() : "");
			
			response.setCard(card);
			response.setData_points(req.getDataPoints());
			response.setErrorMessage("");
			//response.setIdError(0);
			response.setIdUser(idUsuario);
			
			GsonBuilder gsonBuilder = new GsonBuilder();  
			 //gsonBuilder.serializeNulls();  
			 Gson gson = gsonBuilder.create();
			LOGGER.debug("json Gson: " + gson.toJson(req));
			
		}catch(Exception ex){
			LOGGER.error("error al verificar usuario Shift", ex);
			response.setIdError(100);
			if(idioma.equalsIgnoreCase("es"))
				response.setErrorMessage("Error inesperado, intente mas tarde");
			else
				response.setErrorMessage("Unexpected error, try later");
		}
		
		
		LOGGER.debug("RETORNANDO RESPUESTA CHECKUSER: " + GSON.toJson(response));
		return response;
	}
		
}
