package com.addcel.shift.config;

import java.io.IOException;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.addcel.shift.bean.ShiftConfig;
import com.addcel.shift.mapper.JdbcMapper;

public class RestTemplateInterceptor implements ClientHttpRequestInterceptor{

	@Autowired 
	JdbcMapper mapper;
	
	 @Override
	  public ClientHttpResponse intercept(HttpRequest request, byte[] body,
	        ClientHttpRequestExecution execution) throws IOException {
	    HttpHeaders headers = request.getHeaders();
	    
	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON)); 
	    headers.setContentType(MediaType.APPLICATION_JSON); 
	    headers.add("Developer-Autorization", "Bearer CH0MIpa07QhgafzUfxflWInrzeuU2ttQjo5BOZ8CJSdI/wbQ3iV3MvdpJfJOIlf/");
	    headers.add("Project", "Bearer vSGISmhIDNBJR6UMsgy7v9NjeAJUo/88G82i++lRPXckgAt+BgeWgcc6+B5lByYC");
	    headers.add("Trusted-Partner-Autorization", "Bearer 06YXiAKtHIo1tgXBeWYLioKRJhm1W1CZmbqAboTFQ8EiBN3ong8osST7zP4T3DXA");
	    headers.add("Trusted-Partner-ID", "a15b3d15-20a4-79e9-f2da-c8fd78321490");
	    return execution.execute(request, body);
	  }
}
